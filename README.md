# Y2Dashboard
Y2Dashboard Web Application

![Screenshot](./ydashboard.png)

## Configuration

- Copy the `config/bugzilla.yml.template` file to `config/bugzilla.yml` and write
your bugzilla.suse.com credentials there.

## Running the Server

### Initialization

- Install the needed Ruby gems: `bundle install --path vendor/bundle`
- Initalize the database: `bundle exec rake db:migrate`

### Run

To run the server (in development):

```shell
# clear the job queue
bundle exec rake jobs:clear
bundle exec bin/rails s
```

## Delayed Jobs

To run the background jobs with 2 workers run 

```shell
bundle exec bin/delayed_job -n2 start
```
