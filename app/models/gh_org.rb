class GhOrg < ApplicationRecord
  has_many :gh_repos, dependent: :destroy
end
