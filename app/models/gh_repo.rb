class GhRepo < ApplicationRecord
  belongs_to :gh_org
  has_many :gh_issues, dependent: :destroy
end
