class DashboardController < ApplicationController
  def index
    # FIXME: split this too long method
    @bugstat = BugStat.order(:date).last
    @bugstat_old = BugStat.where(date: @bugstat.date - 7).first
    @closed_bugstat = ClosedBugStat.order(:date).last
    @closed_bugstat_old = ClosedBugStat.where(date: @closed_bugstat.date - 7).first
    @y2maintainers = Bug.where(assignee: "yast2-maintainers@suse.de").count
    
    @obs_head_failures = ObsProject.find_by(name: "YaST:Head").obs_repo_statuses.map{|a| a.failures}.sum

    @obs_head_factory_failures = ObsProject.find_by(name: "YaST:Head")
      .obs_repo_statuses.find_by(name: "openSUSE_Factory").failures
    @obs_head_tw_failures = ObsProject.find_by(name: "YaST:Head")
      .obs_repo_statuses.find_by(name: "openSUSE_Tumbleweed").failures
    @obs_head_42_2_failures = ObsProject.find_by(name: "YaST:Head")
      .obs_repo_statuses.find_by(name: "openSUSE_Leap_42.2").failures
    @obs_head_sp1_failures = ObsProject.find_by(name: "YaST:Head")
      .obs_repo_statuses.find_by(name: "SLE_12_SP1").failures
    
    @obs_head_travis_failures = ObsProject.find_by(name: "YaST:Head:Travis")
       .obs_repo_statuses.find_by(name: "xUbuntu_12.04").failures
    @obs_sp2_travis_failures = ObsProject.find_by(name: "YaST:SLE-12:SP2:Travis")
       .obs_repo_statuses.find_by(name: "xUbuntu_12.04").failures
    @obs_42_2_travis_failures = ObsProject.find_by(name: "YaST:openSUSE:42.2:Travis")
       .obs_repo_statuses.find_by(name: "xUbuntu_12.04").failures

    @ibs_head_sp3_failures = ObsProject.find_by(name: "Devel:YaST:Head")
      .obs_repo_statuses.find_by(name: "sle12_sp3").failures
    @ibs_casp1_failures = ObsProject.find_by(name: "Devel:YaST:CASP:1.0")
      .obs_repo_statuses.find_by(name: "CASP_1.0").failures
    @ibs_sp2_failures = ObsProject.find_by(name: "Devel:YaST:SLE-12-SP2")
      .obs_repo_statuses.find_by(name: "SLE_12_SP2").failures

    @pjenkins_y2_failures = JenkinsStatus.where(internal: false, success: false)
      .select{|s| s.name.start_with?("yast-") && s.name.end_with?("-master") }.count
    @pjenkins_yui_failures = JenkinsStatus.where(internal: false, success: false)
      .select{|s| s.name.start_with?("libyui-") && s.name.end_with?("-master") }.count
    @pjenkins_yuistg_failures = JenkinsStatus.where(internal: false, success: false)
      .select{|s| s.name.start_with?("libyui-") && s.name.end_with?("-staging") }.count
      
    @gh_yast_pulls = 0
    @gh_yast_issues = 0
    yast_org = GhOrg.find_by(name: "yast")

    if yast_org
      yast_org.gh_repos.each do |repo|
        @gh_yast_pulls += repo.gh_issues.where(is_pull: true).count
        @gh_yast_issues += repo.gh_issues.where(is_pull: false).count
      end
    end

  end
end
