class ObsController < ApplicationController
  def index
    @statuses = ObsRepoStatus.order(:name)
  end
end
