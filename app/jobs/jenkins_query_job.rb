
require "ydashboard/jenkins_query"
require "ydashboard/jenkins_result_importer"

class JenkinsQueryJob < ApplicationJob
  queue_as :default

  EXTERNAL_JOBS = /\A(yast|libyui)-/

  INTERNAL_JOBS = /\A(yast|libyui)-/

  def perform(*args)
    external_query = Ydashboard::JenkinsQuery.new(job_pattern: EXTERNAL_JOBS, internal_jenkins: false)
    import_jobs(external_query)

    internal_query = Ydashboard::JenkinsQuery.new(job_pattern: INTERNAL_JOBS, internal_jenkins: true)
    import_jobs(internal_query)
  ensure
    # enqueue itself
    JenkinsQueryJob.set(wait: 15.minutes).perform_later
  end

private

  def import_jobs(query)
    importer = Ydashboard::JenkinsResultImporter.new(query)
    importer.import
  end

end
