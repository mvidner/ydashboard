require "ydashboard/obs_status_query"
require "ydashboard/obs_result_importer"

class ObsQueryJob < ApplicationJob
  queue_as :default

  OBS_PROJECTS = [ "YaST:Head", "YaST:Head:Travis", "YaST:openSUSE:42.2",
    "YaST:openSUSE:42.2:Travis", "YaST:SLE-12:SP2", "YaST:SLE-12:SP2:Travis",
    "YaST:storage-ng", "YaST:storage-ng:Travis"]

  IBS_PROJECTS = [ "Devel:YaST:Head", "Devel:YaST:CASP:1.0", "Devel:YaST:SLE-12",
    "Devel:YaST:SLE-12-SP1", "Devel:YaST:SLE-12-SP2" ]

  def perform(*args)
    import_projects(OBS_PROJECTS, false)
    import_projects(IBS_PROJECTS, true)
  ensure
    # enqueue itself in 1 hour
    ObsQueryJob.set(wait: 1.hour).perform_later
  end

private

  def import_projects(projects, internal_obs)
    projects.each do |obs_project|
      query = Ydashboard::ObsStatusQuery.new(obs_project, internal_obs)
      xml = query.run
      obs_importer = Ydashboard::ObsResultImporter.new(xml, internal_obs)
      obs_importer.save!
    end
  end

end
