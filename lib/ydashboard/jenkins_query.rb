
require "jenkins_api_client"

module Ydashboard
  class JenkinsQuery
    PUBLIC_JENKINS = "https://ci.opensuse.org"
    INTERNAL_JENKINS = "https://ci.suse.de"

    attr_reader :job_pattern, :internal_jenkins

    def initialize(job_pattern: //, internal_jenkins: false)
      @job_pattern = job_pattern
      @internal_jenkins = internal_jenkins
    end

    def run
      url = internal_jenkins ? INTERNAL_JENKINS : PUBLIC_JENKINS
      jenkins = JenkinsApi::Client.new(server_url: url)
      all_jobs = jenkins.job.list_all_with_details

      all_jobs.select { |j| j["name"] =~ job_pattern }
    end

  end
end






