
module Ydashboard
  class GhIssuesImporter
    attr_reader :data

    def initialize(query_result)
      @data = query_result
    end

    def save!
      data.each do |org_name, repos|
        GhIssue.transaction do
          gh_org = GhOrg.find_or_create_by(name: org_name)
          # delete all repos, recreate them from scratch to avoid stale repos/issues
          gh_org.gh_repos.delete_all

          repos.each do |repo_name, issues|
            gh_repo = gh_org.gh_repos.create(name: repo_name)
            issues.each do |issue|
              gh_repo.gh_issues.create(
                number: issue[:number],
                is_pull: !issue[:pull_request].nil?,
                title: issue[:title],
                modified_at: issue[:updated_at]
              )
            end
          end
        end
      end
    end
  end
end
