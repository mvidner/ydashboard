
require "octokit"

module Ydashboard
  class GhIssuesQuery
    GITHUB_ORG = "yast"

    attr_reader :org

    def initialize(org = GITHUB_ORG)
      @org = org
    end

    def run
      # return { "yast" => { "yast-yast2" => Octokit.issues("yast/yast-yast2")}}

      # fetch all resources
      Octokit.auto_paginate = true
      result = {}

      Octokit.repos(org).each do |repo|
        repo_name = repo[:name]

        if repo[:open_issues_count] > 0
          issues = Octokit.issues("#{org}/#{repo_name}")
        else
          issues = []
        end

        result[repo_name] = issues
      end

      { org => result }
    end

  end
end






