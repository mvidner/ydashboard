require 'test_helper'

class BugzillaControllerTest < ActionDispatch::IntegrationTest
  test "should get graphs" do
    get bugzilla_graphs_url
    assert_response :success
  end

  test "should get summary" do
    get bugzilla_summary_url
    assert_response :success
  end

  test "should get index" do
    get bugzilla_index_url
    assert_response :success
  end

end
