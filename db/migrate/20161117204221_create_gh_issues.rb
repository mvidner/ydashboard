class CreateGhIssues < ActiveRecord::Migration[5.0]
  def change
    create_table :gh_issues do |t|
      t.integer :number,  null: false
      t.boolean :is_pull,  null: false
      t.string :title,  null: false
      t.datetime :modified_at,  null: false
      t.references :gh_repo, foreign_key: true

      t.timestamps
    end
    add_index :gh_issues, :is_pull
    add_index :gh_issues, :modified_at
  end
end
